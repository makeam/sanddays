# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_02_09_195029) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "categories", force: :cascade do |t|
    t.string "title"
    t.integer "weight"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "day_results", force: :cascade do |t|
    t.date "date"
    t.text "best_event"
    t.text "recognized"
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_day_results_on_user_id"
  end

  create_table "day_works", force: :cascade do |t|
    t.date "day"
    t.bigint "work_id", null: false
    t.integer "result", default: 0
    t.integer "points", default: 0
    t.integer "prev_points", default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["work_id"], name: "index_day_works_on_work_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "avatar"
    t.text "mission"
    t.string "first_name"
    t.string "second_name"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "works", force: :cascade do |t|
    t.string "title"
    t.integer "complexity", default: 1
    t.bigint "user_id", null: false
    t.bigint "category_id"
    t.string "frequency_type", default: "every_day"
    t.integer "frequency", default: 1
    t.text "desc_10"
    t.text "desc_50"
    t.text "desc_100"
    t.boolean "finished", default: false
    t.boolean "started", default: false
    t.date "start_date"
    t.date "finish_date"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "icon", default: "star"
    t.string "color", default: ""
    t.index ["category_id"], name: "index_works_on_category_id"
    t.index ["user_id"], name: "index_works_on_user_id"
  end

  add_foreign_key "day_results", "users"
  add_foreign_key "day_works", "works"
  add_foreign_key "works", "users"
end
