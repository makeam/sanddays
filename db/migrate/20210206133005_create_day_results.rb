class CreateDayResults < ActiveRecord::Migration[6.0]
  def change
    create_table :day_results do |t|
      t.date :date
      t.text :best_event
      t.text :recognized
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
