class CreateDayWorks < ActiveRecord::Migration[6.0]
  def change
    create_table :day_works do |t|
      t.date :day
      t.references :work, null: false, foreign_key: true
      t.integer :result, default: 0
      t.integer :points, default: 0
      t.integer :prev_points, default: 0

      t.timestamps
    end
  end
end
