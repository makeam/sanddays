class AddIconToWorks < ActiveRecord::Migration[6.0]
  def change
    add_column :works, :icon, :string, default: 'star'
    add_column :works, :color, :string, default: ''
  end
end
