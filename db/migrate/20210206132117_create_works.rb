class CreateWorks < ActiveRecord::Migration[6.0]
  def change
    create_table :works do |t|
      t.string :title
      t.integer :complexity, default: 1
      t.references :user, null: false, foreign_key: true
      t.references :category
      t.string :frequency_type, default: 'every_day'
      t.integer :frequency, default: 1
      t.text :desc_10
      t.text :desc_50
      t.text :desc_100
      t.boolean :finished, default: false
      t.boolean :started, default: false
      t.date :start_date
      t.date :finish_date

      t.timestamps
    end
  end
end
