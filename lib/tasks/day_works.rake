namespace :day_works do
  task :generate => :environment do
    puts "Day results: Create day results for 1 month in the future"
    User.find_each do |user|
      puts "User: #{user.email}"
      user.works.each do |work|
        puts "Habit: #{work.title}"
        work.create_day_works
      end
    end
    puts "Successful."
  end

  task :check => :environment do
    puts "Check day results"
    DayWork.unchecked.find_each do |day_work|
      puts "DayWork: #{day_work.id}"
      day_work.check_undone!
    end
    puts "Successful."
  end
end
