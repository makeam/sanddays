class DayWork < ApplicationRecord
  validates :day, presence: true, uniqueness: { scope: [:work_id] }
  belongs_to :work

  before_validation :calc_points, on: :update
  # after_commit :update_next_day,  on: :update

  def calc_points
    self.points = work.calc_points(result)
  end

  # def update_next_day
  #   next_day_works.first.update(prev_points: total_points) if next_day_works.first
  # end

  def next_day_works
    work.day_works.where("day > ?", day).order(:day)
  end

  def check_undone!
    self.update(points: work.calc_points(0), result: 0) if (result == -1) && (day < Date.current)
  end

  def self.unchecked(day = Date.current)
    where("day < ? AND result = -1", day)
  end

  def total_points
    pp = prev_points || 0
    p  = points      || 0
    pp + p
  end

end
