class Work < ApplicationRecord
  validates :title, presence: true

  belongs_to :category, optional: true
  belongs_to :user
  has_many :day_works, dependent: :destroy

  extend Enumerize
  enumerize :frequency_type, :in => {
    every_day:   1,
    every_week:  2,
    every_month: 3,
    in_week:     4,
    in_month:    5
  }


  FAIL_DAY_POINTS = -1
  DAY_POINTS = 1.0


  before_validation :check_params
  after_commit :create_day_works

  def check_params
    self.frequency = 1  if frequency.nil? || frequency == 0
    self.complexity = 1 if complexity.nil? || complexity == 0
  end

  def last_day_works(count=1)
    day_works.where("day <= ?", Date.current).order(:day).last(count)
  end
  
  def create_day_works(range = nil)
    range = get_creation_range if range == nil

    last_dw = day_works.order(:day).last

    range.each do |day|
      DayWork.create(
        day: day,
        work: self,
        result: -1,
        points: day < Date.current ? Work::FAIL_DAY_POINTS : 0,
        prev_points: last_dw ? last_dw.prev_points + last_dw.points : 0
      )
    end
  end

  def get_creation_range
    last_dw = day_works.order(:day).last
    start = last_dw ? last_dw.day + 1.day : Date.current
    finish = Date.current + 1.month
    start..finish
  end

  def calc_points(result)
    result = result.to_i
    return Work::FAIL_DAY_POINTS if result == 0
    return base_points     if result == 1
    return base_points + 2 if result == 2
    return base_points + 4 if result == 3
    0
  end

  def base_points
    self.frequency = 1 if self.frequency.nil? || self.frequency < 1
    return Work::DAY_POINTS * self.frequency * complexity      if frequency_type == 'every_day'
    return Work::DAY_POINTS * self.frequency * complexity * 7  if frequency_type == 'every_week'
    return Work::DAY_POINTS * self.frequency * complexity * 30 if frequency_type == 'every_month'
    return Work::DAY_POINTS / self.frequency * complexity * 7  if frequency_type == 'in_week'
    return Work::DAY_POINTS / self.frequency * complexity * 30 if frequency_type == 'in_month'
    0
  end




end
