# frozen_string_literal: true

class Ability
  include CanCan::Ability

  def initialize(user)
    if user
      @user = user
      user_abilities
    else
      guest_abilities
    end
  end

  private

  def guest_abilities; end

  def user_abilities
    can :manage, @user
    can :manage, Work, user_id: @user.id
    can :manage, DayWork, work: { user_id: @user.id }
  end
end
