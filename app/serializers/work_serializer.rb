class WorkSerializer
  include JSONAPI::Serializer
  attributes :id,
             :title,
             :frequency_type,
             :frequency,
             :complexity,
             :desc_10,
             :desc_50,
             :desc_100,
             :icon,
             :color

  attribute :last_points do |work|
    dw_points = []
    work.last_day_works(14).each do |dw|
      dw_points << {
        day: dw.day,
        points: dw.points
      }
    end
    dw_points
  end
end
