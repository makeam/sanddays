class ProfileSerializer
  include JSONAPI::Serializer
  attributes :email
end
