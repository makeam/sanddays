class DayWorkSerializer
  include JSONAPI::Serializer
  attributes :id,
             :day,
             :work_id,
             :points,
             :prev_points,
             :result

  attribute :total_points do |dw|
    dw.total_points
  end

end
