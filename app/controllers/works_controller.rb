class WorksController < ApplicationController
  before_action :authenticate_user!
  respond_to :json

  def index
    authorize! :index, Work
    @works = Work.accessible_by(current_ability)
    render json: WorkSerializer.new(@works).serializable_hash.to_json, status: :ok
  end

  def create
    authorize! :create, Work
    @work = Work.create(work_params.merge(user: current_user))
    if @work.errors.empty?
      render json: WorkSerializer.new(@work).serializable_hash.to_json, status: :ok
    else
      render json: @work.errors.full_messages.to_json, status: :unprocessable_entity
    end
  end

  def update
    @work = Work.find(params[:id])
    authorize! :update, @work
    if @work.update(work_params)
      render json: WorkSerializer.new(@work).serializable_hash.to_json, status: :ok
    else
      render json: @work.errors.full_messages.to_json, status: :unprocessable_entity
    end
  end

  private

  def work_params
    params.require(:work).permit(
      :title,
      :complexity,
      :category_id,
      :frequency_type,
      :frequency,
      :desc_10,
      :desc_50,
      :desc_100,
      :finished,
      :started,
      :start_date,
      :finish_date,
      :icon,
      :color
    )
  end

end