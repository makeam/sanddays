class DayWorksController < ApplicationController
  before_action :authenticate_user!
  respond_to :json

  def index
    authorize! :index, DayWork
    @day_works = DayWork.accessible_by(current_ability)
    render json: DayWorkSerializer.new(@day_works).serializable_hash.to_json, status: :ok
  end

  def show
    @day_work = DayWork.where(day: params[:day].to_date, work_id: params[:work_id]).first
    authorize! :show, @day_work
    render json: DayWorkSerializer.new(@day_work).serializable_hash.to_json, status: :ok
  end

  def create
    authorize! :create, DayWork
    @day_work = DayWork.create(day_work_params.merge(user: current_user))
    if @day_work.errors.empty?
      render json: DayWorkSerializer.new(@day_work).serializable_hash.to_json, status: :ok
    else
      render json: @day_work.errors.full_messages.to_json, status: :unprocessable_entity
    end
  end

  def update
    @day_work = DayWork.find(params[:id])
    authorize! :update, @day_work
    if @day_work.update(day_work_params)
      render json: DayWorkSerializer.new(@day_work).serializable_hash.to_json, status: :ok
    else
      render json: @day_work.errors.full_messages.to_json, status: :unprocessable_entity
    end
  end

  private

  def day_work_params
    params.require(:day_work).permit( :result )
  end

end