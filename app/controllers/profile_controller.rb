class ProfileController < ApplicationController
  before_action :authenticate_user!
  respond_to :json
  def show
    render json: ProfileSerializer.new(current_user).serializable_hash.to_json, status: 200
  end
end