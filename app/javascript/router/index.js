import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter);

import Dashboard from '../components/dashboard/dashboard'
import Syllable from '../components/syllables/syllables'
import QuestList from "../components/quests/QuestList";
import WorksList from '../components/works/works_list'
import WorkForm from '../components/works/work_form'

const routes = [
  { path: '/', component: Dashboard },
  { path: '/syllables', component: Syllable },
  { path: '/quests', component: QuestList },
  { path: '/works', component: WorksList,
    children: [
      {
        path: '/works/new',
        component: WorkForm,
        name: 'new_work',
        props: true,
      },
      {
        path: '/works/:id',
        component: WorkForm,
        name: 'work_form',
        props: true,
      },

    ]
  },
  // { path: '/messages', component: MessagesList },
]

const router = new VueRouter({
  mode: 'history',
  routes // short for `routes: routes`
})
export default router
