// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.

// require("@rails/ujs").start()
// require("@rails/activestorage").start()
// require("channels")


// Uncomment to copy all static images under ../images to the output folder and reference
// them with the image_pack_tag helper in views (e.g <%= image_pack_tag 'rails.png' %>)
// or the `imagePath` JavaScript helper below.
//
// const images = require.context('../images', true)
// const imagePath = (name) => images(name, true)
console.log('Webpacker compiled - App.');

import Vue from 'vue'
import App from '../app/app.vue'
import 'stylesheets/application'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import _ from 'lodash'

var moment = require('moment');
moment.defaultFormat= 'YYYY-MM-DD';
moment.locale('ru');

import '../api'

Vue.use(BootstrapVue);
// import vuetify from '../plugins/vuetify' // path to vuetify export
import router from '../router'
import store from '../store'

Vue.component('app', App);

document.addEventListener('DOMContentLoaded', () => {
  new Vue({
    router,
    store,
  }).$mount('#app')
})
