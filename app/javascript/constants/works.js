const WORKS = [
  {
    id: 1,
    attributes:{
      id: 1,
      title: 'my work 1',
      category_id: 1,
      complexity: 1,
      frequency_type: 'day',
      frequency: 1,
      desc_10: '',
      desc_50: '',
      desc_100: '',
    }
  },
  {
    id: 2,
    attributes:{
      id: 2,
      title: 'my work 2',
      category_id: 1,
      complexity: 1,
      frequency_type: 'day',
      frequency: 1,
      desc_10: '',
      desc_50: '',
      desc_100: '',
    }
  },
]
export {WORKS}