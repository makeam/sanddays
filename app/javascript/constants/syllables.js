const SYLLABLES = [
  'MA',
  'МУ',
  'МИ',
  'МЕ',
];

const WRONG_SYLLABLES = [
  'ЧЭ',
  'ЦЭ',
  'ШЭ',
  'ЩЭ',
  'ЧЮ',
  'ЦЮ',
  'ШЮ',
  'ЩЮ',
  'ФЮ',
  'ХЮ',
  'ЩЫ',
  'ШЫ',
  'ЖЫ',
  'ЧЫ',
  'ЧЯ',
  'ХЯ',
  'ФЯ',
  'ЦЯ',
  'ШЯ',
  'ЩЯ',
  'ЩО',
  'ЦЁ',

];

const VOWEL_LETTERS = [
  {letter: 'А', css: 'vowel-1'},
  {letter: 'О', css: 'vowel-2'},
  {letter: 'И', css: 'vowel-3'},
  {letter: 'У', css: 'vowel-4'},
  {letter: 'Е', css: 'vowel-5'},
  {letter: 'Я', css: 'vowel-6'},
  {letter: 'Ё', css: 'vowel-7'},
  {letter: 'Ы', css: 'vowel-8'},
  {letter: 'Ю', css: 'vowel-9'},
  {letter: 'Э', css: 'vowel-10'},
]//'АЯУЮЫИЭЕОЁ';
const CONSONANT_LETTERS = 'БВГДЖЗКЛМНПРСТФХЦЧШЩ';
const EXTRA_LETTERS = 'Й';
const SOFT_SIGN = 'Ь';
const HARD_SIGN = 'Ъ';


export {
  SYLLABLES,
  VOWEL_LETTERS,
  CONSONANT_LETTERS,
  WRONG_SYLLABLES,
}