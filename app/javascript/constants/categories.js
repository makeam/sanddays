const CATEGORIES = [
  {
    id: 1,
    attributes: {
      id: 1,
      title: 'Утро'
    },
  },
  {
    id: 2,
    attributes: {
      id: 2,
      title: 'День'
    },
  },
]

export {CATEGORIES}