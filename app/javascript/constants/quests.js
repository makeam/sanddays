const QUESTS_MATH = [
  {
    id: 1,
    mode: 'math',
    text: '2 + 5 = ?',
    answers: {
      mode: 'text',
      size: 'lg',
      items: [
        {
          right: true,
          content: '7'
        },
        {
          right: false,
          content: '9'
        },
        {
          right: false,
          content: '6'
        },
      ]
    }
  },
  {
    id: 2,
    mode: 'math',
    text: '7 + 4 = ?',
    answers: {
      mode: 'text',
      size: 'lg',
      items: [
        {
          right: true,
          content: '11'
        },
        {
          right: false,
          content: '12'
        },
        {
          right: false,
          content: '9'
        },
      ]
    }
  },
  {
    id: 3,
    mode: 'math',
    text: '9 - 5 = ?',
    answers: {
      mode: 'text',
      size: 'lg',
      items: [
        {
          right: true,
          content: '4'
        },
        {
          right: false,
          content: '7'
        },
        {
          right: false,
          content: '3'
        },
      ]
    }
  },
];

const QUEST_TYPES = [
  'MATH',
  'REED',
  'QUESTION',
]

export {
  QUEST_TYPES,
  QUESTS_MATH,
}