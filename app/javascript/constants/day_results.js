const DAY_RESULTS = [
  {
    id: 1,
    attributes: {
      id: 1,
      day: '01-02-2021',
      work_id: 1,
      result: 0,
      points: 2,
      prev_points: 0,
    }
  },
  {
    id: 2,
    attributes: {
      id: 2,
      day: '02-02-2021',
      work_id: 1,
      result: 0,
      points: 2,
      prev_points: 2,
    }
  },
  {
    id: 3,
    attributes: {
      id: 3,
      day: '03-02-2021',
      work_id: 1,
      result: 0,
      points: 2,
      prev_points: 4,
    }
  },
]

export {DAY_RESULTS}