// import Vue from 'vue'
import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'

const _opts = {};
const opts = {
  //customVariables: ['../src/sass/variables.scss'],
  // defaultAssets: false,
  theme: {
    dark: false,

  },
  defaultAssets: {
    font: {
      family: 'Rubik'
    }
  },
  //treeShake: true,
  icons: {
    iconfont: 'mdiSvg',
  }
}
Vue.use(Vuetify)


export default new Vuetify(opts)