import Vue from 'vue';
import axios from 'axios';

//csrf
const token = document.querySelector('meta[name=csrf-token]').content
const SERVER_DAY_FORMAT = 'DD-MM-YYYY'
const adapter = axios.create({
  baseURL: '/',
  headers: {
    'Accept': 'application/json',
    'X-CSRF-TOKEN': token,
  }
});

const api = {
  api_prop: {
     server_day_format: SERVER_DAY_FORMAT,
  },
  profile:{
    info: () => adapter.get('profile'),
    update: (payload) => adapter.patch('profile', payload)
  },
  works: {
    index: () => adapter.get('works'),
    create: (payload) => adapter.post(`works`, payload),
    update: (id, payload) => adapter.patch(`works/${id}`, payload),
  },
  day_works: {
    show: (work_id, day) => adapter.get(`works/${work_id}/day_works/${day}`),
    update: (id, payload) => adapter.patch(`day_works/${id}`, payload),
  }
};

Vue.prototype.$api = api;