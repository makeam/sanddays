import Vue from 'vue'

import {UI_ICONS} from "../../constants/icons";

const API = Vue.prototype.$api

const state = {
  icons: UI_ICONS,
}

const getters = {
  icon: state => (icon_name) => {
    return state.icons[icon_name];
  },
}

const actions = {
}

const mutations = {
}


export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
