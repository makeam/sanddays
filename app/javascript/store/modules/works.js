import Vue from 'vue'
import {WORKS} from "../../constants/works";
import {ICONS, RESULT_ICONS} from "../../constants/icons";

const API = Vue.prototype.$api

const state = {
  works: [],
  icons: ICONS,
  result_icons: RESULT_ICONS,
}

const getters = {
  all_works: state => {
    return state.works;
  },
  icon: state => (icon_name) => {
    return state.icons[icon_name];
  },
  work: state => (id) => {
    return _.find(state.works, (w) => { return String(w.id) === String(id) });
  },
}

const actions = {
  get_works({dispatch, commit}, payload){
    API.works.index()
      .then((res) => {
        commit('set_works', res.data.data);
      })
      .catch((error) => {
        // handle error
        console.log(error);
      })
      .then(() => {
        // always executed
        this.loading = false;
      });

    // commit('set_works', WORKS);
  },

  update_work({dispatch, commit}, payload){
    API.works.update(payload.id, payload.params)
      .then((res) => {
        commit('update_work', res.data.data);
      })
      .catch((error) => {
        // handle error
        console.log(error);
      })
      .then(() => {
        // always executed
        this.loading = false;
      });
  },

  create_work({dispatch, commit}, payload){
    API.works.create(payload)
      .then((res) => {
        commit('add_work', res.data.data);
      })
      .catch((error) => {
        // handle error
        console.log(error);
      })
      .then(() => {
        // always executed
        this.loading = false;
      });
  },

}

const mutations = {
  set_works(state, works){
    state.works = works;
  },

  add_work(state, work){
    state.works.push(work);
  },

  update_work(state, work){
    let index = _.findIndex(state.works, {id: work.id});
    if (index > -1) {
      state.works.splice(index, 1, work);
    } else {
      //обработка ошибок
      console.error('Элемент не найден - ID:', work.id );
    }
  }
}


export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
