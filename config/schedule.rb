# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever
# Custom template for LOCUM
ruby_version = ENV["RUBY_VERSION"] ? ENV["RUBY_VERSION"] : '2.6'
custom_template = "rvm #{ruby_version} do bundle exec rake :task --silent :output"

job_type :rake,   "cd :path && :environment_variable=:environment #{custom_template}"

every 1.day, :at => '00:01 am' do
  rake '-s day_works:check'
end

every 1.day, :at => '05:00 am' do
  rake '-s day_works:generate'
end
