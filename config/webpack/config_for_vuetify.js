// webpack.config.js
// this copied from Vuetify docs
module.exports = {
  module: {
    rules: [
      {
        test: /\.styl$/,
        loader: ['css-loader', 'stylus-loader']
      },
      {
        test: /\.s(c|a)ss$/,
        use: [
          'vue-style-loader',
          'css-loader',
          {
            loader: 'sass-loader',
            options: {
              implementation: require('sass'),
              sassOptions: {
                indentedSyntax: true, // optional
                fiber: false,
              },
            },
          },
        ],
      },
    ],
  }
}