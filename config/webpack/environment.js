const { environment } = require('@rails/webpacker')
const { VueLoaderPlugin } = require('vue-loader')
const vue = require('./loaders/vue')
const pug_loader = require('./loaders/pug')
const configVuetify = require('./config_for_vuetify')
const customConfig = require('./custom')
//
environment.config.merge(customConfig)
// environment.config.merge(configVuetify)
environment.plugins.prepend('VueLoaderPlugin', new VueLoaderPlugin())
environment.loaders.prepend('vue', vue)
environment.loaders.prepend('pug', pug_loader)
///////////////////////////
// sass-loader configs
const sassLoader = environment.loaders.get('sass').use.find(item => item.loader === 'sass-loader')

// use dart sass
sassLoader.options.implementation = require('sass')
sassLoader.options.sassOptions = {}
// use fibers
sassLoader.options.sassOptions.fiber = require('fibers')
// syntax
sassLoader.options.sassOptions.indentedSyntax = true
// urls
environment.loaders.get('sass').use.splice(-1, 0, {
  loader: 'resolve-url-loader'
});
// console.log('environment.loaders ====================');
// console.log(environment.loaders.get('sass').use);
// environment.config.merge(customConfig);
// console.log(environment.loaders);

/**
 * доступные опции dart sass
 * @see https://github.com/sass/dart-sass#javascript-api
 */
sassLoader.options.sourceMap = true

///////////////////////////
module.exports = environment
