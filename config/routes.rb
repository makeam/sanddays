Rails.application.routes.draw do
  devise_for :users

  # hide crud rails routes for non-json request
  constraints ->(request) { request.format == :json } do
    # ... routes
    get '/profile', to: 'profile#show', as: :profile
    resources :day_works, only: [:update]
    resources :works, only: [:index, :create, :update] do
      resources :day_works, only: [:index, :create, :update] do
        get '/:day', to: 'day_works#show', on: :collection
      end
    end
  end

  # all requests receiving vue app
  root 'home#index'
  get '/*slug', to: 'home#index'
end
